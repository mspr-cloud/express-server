import express from 'express';
import commits from './commits.ctrl';

export default () => {
  const router = express.Router();
  router.route('/organisation/:repository/commits').get(commits.list);
  return router;
};
