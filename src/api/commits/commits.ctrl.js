import CommitsDa from './commits.da';

export default {
  list
};

function list(req, res, next) {
  let start, end;

  if (!req.query.start || !req.query.end) {
    start = new Date();
    end = new Date(new Date().setFullYear(new Date().getFullYear() - 1))
  } else {
    start = new Date(req.query.start);
    end = new Date(req.query.end);
  }

  CommitsDa.findOrganisationRepositoryCommits(req.query.page, req.params.repository, start, end).then((response) => {
    res.status(200).json(response);
  })
}
