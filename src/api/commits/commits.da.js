import Q from 'q';
import { DB } from '../../database';

const LIMIT_QUERY = 100;

export default {
  findOrganisationRepositoryCommits
};

async function findOrganisationRepositoryCommits(page, repositoryName, start, end) {

  let PAGE = parseInt(page) || 0;

  const deferredRepository = Q.defer();
  const deferredCommits = Q.defer();
  const deferredCountDocument = Q.defer();

  DB.$repositories.find({type: "organisation", name: repositoryName})
    .limit(1).toArray(function(err, data) {
      deferredRepository.resolve(data[0]);
  });

  deferredRepository.promise.then((repository) => {

    const QUERY = {
      repository_id: repository._id,
      committedDate: { $gte: start, $lte: end}
    };

    DB.$commits.find(QUERY)
      .skip(LIMIT_QUERY * PAGE)
      .sort({committedDate: -1})
      .limit(LIMIT_QUERY).toArray(function(err, data) {

      deferredCommits.resolve(data);
    });

    DB.$commits.count(QUERY, function(err, data) {
      deferredCountDocument.resolve(data);
    });

  })



  const deferred = Q.defer();
  Q.all([deferredCommits.promise, deferredCountDocument.promise]).then((results) => {
    const  [commits, countDocument] = results;
    deferred.resolve({
      commits,
      hasNextPage: commits.length > 0 ? ((LIMIT_QUERY * PAGE) + commits.length) !== countDocument : false
    });
  });

  return deferred.promise;
}

