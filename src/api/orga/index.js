import express from 'express';
import orga from './orga.ctrl';

export default () => {
  const router = express.Router();
  router.route('/info').get(orga.info);
  return router;
};
