import { DB } from '../../database';

export default {
  info
};

function info(req, res, next) {
  DB.$organisation.find().limit(1).toArray(function (err, orga) {
    if(err) { res.status(422).json(err); }
    res.status(200).json(orga[0]);
  })
}
