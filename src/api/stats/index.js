import express from 'express';
import stats from './stats.ctrl';

export default () => {
  const router = express.Router();
  router.route('/organisation').get(stats.info);
  router.route('/organisation/language/trends').get(stats.organisationLanguageTrends);
  router.route('/projects/latest').get(stats.latestProjects);
  router.route('/projects/random').get(stats.randomProjects);
  router.route('/users/mostStars').get(stats.usersMostStars);
  return router;
};
