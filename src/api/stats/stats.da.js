import Q from 'q';
import { DB } from '../../database';
import { getLastSeedDate } from '../../helper/query'

export default {
  findStatsOrganisation,
  findLatestProjects,
  findRandomProject,
  findUsersMostStars,
  findOrganisationLanguageTrends
};


async function findStatsOrganisation() {
  const deferred = Q.defer();
  DB.$statsOrganisation.find({})
    .sort({"date": -1}).limit(1).toArray(function(err, data) {
    deferred.resolve(data[0]);
  });
  return deferred.promise;
}

async function findLatestProjects() {
  const deferredUser = Q.defer();
  const deferredOrganisation = Q.defer();

  const lastSeed = await getLastSeedDate();

  DB.$repositories.find({"type": "user", "date": lastSeed}).sort({createdAt:-1})
    .limit(5).toArray(function(err, data) {
    deferredUser.resolve(data);
  });

  DB.$repositories.find({"type": "organisation", "date": lastSeed}).sort({createdAt:-1})
    .limit(5).toArray(function(err, data) {
    deferredOrganisation.resolve(data);
  });


  const deferred = Q.defer();
  Q.all([deferredUser.promise, deferredOrganisation.promise]).then((results) => {
    const  [users, organisation] = results;
    deferred.resolve({
      users,
      organisation
    });
  });

  return deferred.promise;
}

async function findRandomProject() {
  const deferred = Q.defer();
  DB.$repositories.aggregate([{ $sample: { size: 1000 } }])
    .limit(3).toArray(function(err, data) {
    deferred.resolve(data);
  });
  return deferred.promise;
}

async function findUsersMostStars() {
  const deferredUsersStarTrendsRepositories = Q.defer();

  const lastSeed = await getLastSeedDate();

  DB.$statsUsersStarTrendsRepositories.find({"date": lastSeed})
    .sort({totalStars:-1}).limit(6).toArray(function(err, data) {
    deferredUsersStarTrendsRepositories.resolve(data);
  });

  let promises;
  const deferred = Q.defer();

  deferredUsersStarTrendsRepositories.promise.then((repositoriesTrends) => {
    promises = repositoriesTrends.map((repositoryTrends) => {
      let deferred = Q.defer();
      DB.$statsUsersStarTrends.find({ _id: repositoryTrends.stats_users_star_trends_id })
        .limit(1).toArray(function(err, data) {
        const user = data[0];
        deferred.resolve(Object.assign(user, {repository: repositoryTrends}))
      });
      return deferred.promise;
    });
    Q.all(promises).then((results) => {
      deferred.resolve(results);
    });
  });

  return deferred.promise;
}

async function findOrganisationLanguageTrends() {
  const deferred = Q.defer();

  DB.$statsOrganisationLanguageTrends.find({})
    .sort({"date": -1}).limit(1).toArray(function(err, data) {
    deferred.resolve(data[0]);
  });

  return deferred.promise;
}
