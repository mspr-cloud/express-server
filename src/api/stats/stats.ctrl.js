import StatsDa from './stats.da';

export default {
  info,
  latestProjects,
  randomProjects,
  usersMostStars,
  organisationLanguageTrends
};

function info(req, res, next) {
  StatsDa.findStatsOrganisation().then((statsOrganisation) => {
    res.status(200).json(statsOrganisation);
  })
}

function latestProjects(req, res, next) {
  StatsDa.findLatestProjects()
    .then((projects) => {
      res.status(200)
        .json(projects);
    })
}

function randomProjects(req, res, next) {
  StatsDa.findRandomProject()
    .then((projects) => {
      res.status(200)
        .json(projects);
    })
}

function usersMostStars(req, res, next) {
  StatsDa.findUsersMostStars()
    .then((users) => {
      res.status(200)
        .json(users);
    })
}

function organisationLanguageTrends(req, res, next) {
  StatsDa.findOrganisationLanguageTrends()
    .then((languageTrends) => {
      res.status(200)
        .json(languageTrends);
    })
}
