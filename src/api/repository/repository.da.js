import Q from 'q';
import { DB } from '../../database';
import { getLastSeedDate } from '../../helper/query'

const LIMIT_QUERY = 50;

export default {
  findOrganisationRepositories
};

async function findOrganisationRepositories(page) {

  let PAGE = parseInt(page) || 0;
  const QUERY = {type: "organisation"};
  const lastSeed = await getLastSeedDate();
  const finalQuery = Object.assign(QUERY, {date: lastSeed});

  const deferredRepository = Q.defer();
  const deferredCountDocument = Q.defer();

  DB.$repositories.find(finalQuery)
    .sort({createdAt: -1})
    .skip(LIMIT_QUERY * PAGE)
    .limit(LIMIT_QUERY).toArray(function(err, data) {
    deferredRepository.resolve(data);
  });



  DB.$repositories.count(finalQuery, function(err, data) {
    deferredCountDocument.resolve(data);
  });


  const deferred = Q.defer();
  Q.all([deferredRepository.promise, deferredCountDocument.promise]).then((results) => {
    const  [repositories, countDocument] = results;
    deferred.resolve({
      repositories,
      hasNextPage: repositories.length > 0 ? ((LIMIT_QUERY * PAGE) + repositories.length) !== countDocument : false
    });
  });

  return deferred.promise;
}

