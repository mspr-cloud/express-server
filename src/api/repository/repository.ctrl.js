import { DB } from '../../database';
import RepositoryDa from './repository.da';

export default {
  list
};

function list(req, res, next) {
  RepositoryDa.findOrganisationRepositories(req.query.page).then((response) => {
    res.status(200).json(response);
  })
}
