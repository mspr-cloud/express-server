import express from 'express';
import repository from './repository.ctrl';

export default () => {
  const router = express.Router();
  router.route('/organisation/list').get(repository.list);
  return router;
};
