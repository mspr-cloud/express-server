import UserDa from '../user/user.da';
import aclStore from '../../helper/acl-store';

export default {
  login,
  signup,
  refresh
};

function login(req, res, next) {
  const { email, password } = req.body;

  UserDa.getByEmail(email)
    .then(user => {
      user.authenticate(password, isMatch => {
        if (!isMatch) {
          res.status(422);
        } else {
          user.generateToken(user, ({token, refreshToken}) => {
            res.status(200).json({ token, refreshToken });
          });
        }
        next();
      });
    })
    .catch(err => {
      res.status(422).json(err);
    });
}

function signup(req, res) {

  const { email, password, firstName, lastName } = req.body;

  UserDa.create(email, password, firstName, lastName)
    .then(user => {
      aclStore.acl.addUserRoles(user._id.toString(), user.role, err => {
        if (!err) {
          user.generateToken(user, ({token, refreshToken}) => {
            res.status(200).json({ token, refreshToken });
          });
        }
      });
    })
    .catch(err => {
      res.status(422).json(err);
    });
}


function refresh(req, res) {

  const { refreshToken } = req.body;
  UserDa.getByRefreshToken(refreshToken)
    .then(user => {
      user.generateToken(user, ({token, refreshToken}) => {
        res.status(200).json({ token, refreshToken });
      });
    })
    .catch(err => {
      res.status(422).json(err);
  });
}
