import Q from 'q';
import User from './user.model';
import aclStore from '../../helper/acl-store';

export default {
  getById,
  getByEmail,
  getByRefreshToken,
  getUserRole,
  addUserRole,
  removeUserRole,
  create,
  remove,
  aclInfo,
  findAll,
};

function getById(id) {
  const deferred = Q.defer();
  User.findOne({ "_id": id }, (err, user) => {
    if (err) deferred.reject(err);

    deferred.resolve(user);
  });

  return deferred.promise;
}

function getByEmail(email) {
  const deferred = Q.defer();
  User.findOne({ email }, (err, user) => {
    if (err) deferred.reject(err);

    deferred.resolve(user);
  });

  return deferred.promise;
}

function getByRefreshToken(refreshToken) {
  const deferred = Q.defer();

  User.findOne({refreshToken: refreshToken}).then((res, err) => {
    console.log(res, err)
  })

  User.findOne({ refreshToken }, (err, user) => {
    if (err) deferred.reject(err);

    deferred.resolve(user);
  });

  return deferred.promise;
}

function create(email, password, firstName, lastName, role) {
  const deferred = Q.defer();

  const newUser = new User({
    email,
    password,
    firstName,
    lastName,
    role
  });

  newUser.save((err, user) => {
    if (err) deferred.reject(err);

    deferred.resolve(user);
  });

  return deferred.promise;
}

function remove(id) {
  const deferred = Q.defer();
  User.remove({ _id: id }, (err, user) => {
    if (err) deferred.reject(err);

    deferred.resolve(user);
  });

  return deferred.promise;
}


function getUserRole (target) {
  const { acl } = aclStore;
  const deferred = Q.defer();

  acl.userRoles(target, (err, roles) => {
    if (err) deferred.reject(err);

    deferred.resolve(roles);
  });

  return deferred.promise;
}

function removeUserRole(target, role) {
  const { acl } = aclStore;
  const deferred = Q.defer();

  acl.removeUserRoles(target, role, (err) => {
    if (err) deferred.reject(err);

    deferred.resolve();
  });

  return deferred.promise;
}

function addUserRole (target, role) {
  const { acl } = aclStore;
  const deferred = Q.defer();

  acl.addUserRoles(target, role, (err) => {
    if (err) deferred.reject(err);

    deferred.resolve();
  });

  return deferred.promise;
}


function aclInfo (target) {

  const { acl } = aclStore;
  const deferred = Q.defer();

  getById(target).then((user) => {
    acl.whatResources(user.role, (err, res) => {
      if (err) deferred.reject(err);
      deferred.resolve(res);
    })
  })

  return deferred.promise;
}

function findAll() {
  const deferred = Q.defer();

  User.find({}, function(err, users) {
    if (err) deferred.reject(err);

    deferred.resolve(users);
  });

  return deferred.promise;
}
