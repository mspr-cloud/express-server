import UserDa from '../user/user.da';

export default {
  me,
  updateRole,
  aclInfo,
  list,
  deleteUser
};

function me(req, res, next) {
  const user = req.user;

  res.status(200).json(user);
}

function updateRole(req, res, next) {
  const  target = req.params.uid;
  const { role } = req.body;

  UserDa.getById(target).then((user) => {
    UserDa.getUserRole(target).then((userRole) => {
      UserDa.removeUserRole(target, userRole).then(() => {
        UserDa.addUserRole(target, role).then(() => {
          user.role = role;
          user.save();
          res.status(200).json(user);
        })
      })
    })
  })
}

function list(req, res, next) {
  UserDa.findAll().then((users) => {
    res.status(200).json(users)
  })
}

function aclInfo(req, res, next) {
  const user = req.user;

  UserDa.aclInfo(user._id.toString()).then((authorization) => {
    res.status(200).json(authorization)
  })
}

function deleteUser(req, res, next) {
  const target = req.params.uid;

  UserDa.remove(target).then((user) => {
    res.status(200).json(user)
  })
}
