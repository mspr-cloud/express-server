import express from 'express';
import ctrl from './user.ctrl';
import aclMiddleware from '../../middleware/acl';

export default () => {
  const router = express.Router();

  router.route('/me').get(ctrl.me);
  router.route('/acl/info').get(ctrl.aclInfo);


  /**
   * Protected
   */
  router.route('/:uid/role').post(aclMiddleware, ctrl.updateRole);
  router.route('/:uid/delete').post(aclMiddleware, ctrl.deleteUser);
  router.route('/list').get(aclMiddleware, ctrl.list);


  return router;
};
