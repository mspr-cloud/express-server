import { MongoClient, ObjectID } from 'mongodb';
import config from './config/config';

class Database {

  constructor() {
    this.$commits = {};
    this.$repositories = {};
    this.$organisationUsers = {};
    this.$organisation = {};

    this.$statsOrganisation = {};
    this.$statsOrganisationLanguageTrends = {};
    this.$statsUsersStarTrends = {};
    this.$statsUsersStarTrendsRepositories = {};

    this.$seedHistory = {};
  }

  async init() {
    if(this.client) return;

    try {
      this.client = await MongoClient.connect(config.db);
      this.db = await this.client.db();
    } catch(err) {
      console.error(err);
      process.exit(0);
    }

    this.$organisationUsers = this.db.collection('organisation_users');
    this.$repositories = this.db.collection('repositories');
    this.$organisation = this.db.collection('organisation');
    this.$commits = this.db.collection('commits');

    /** STATS **/
    this.$statsUsersStarTrends = this.db.collection('stats_users_star_trends');
    this.$statsUsersStarTrendsRepositories = this.db.collection('stats_users_star_trends_repositories');
    this.$statsOrganisationLanguageTrends = this.db.collection('stats_organisation_language_trends');
    this.$statsOrganisation = this.db.collection('stats_organisation');

    this.$seedHistory = this.db.collection('seed_history');
  }
}

export const DB = new Database();
