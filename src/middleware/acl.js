import aclStore from '../helper/acl-store';

export default (req, res, next) => {
  const { acl } = aclStore;
  let ressource;
  const userId = req.user ? req.user.id.toString() : 'guest';

  if (req.route.path === '*') {
    ressource = req.baseUrl;
  } else {
    ressource = req.baseUrl + req.route.path;
  }

  acl.isAllowed(userId, ressource, req.method.toLowerCase(), (err, result) => {
    if (result) return next()

    return res.status(403).send('You are not permitted to perform this action')
  })
}
