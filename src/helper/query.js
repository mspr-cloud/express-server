import Q from 'q';
import { DB } from '../database';

export async function getLastSeedDate () {
  const deferred = Q.defer();
  DB.$seedHistory.find({})
    .sort({"date": -1}).limit(1).toArray(function(err, data) {
    deferred.resolve(data[0].date);
  });
  return deferred.promise;
}
