import { REPOSITORY_FRAGMENT } from './fragments/repository.js'
import { COMMIT_FRAGMENT } from './fragments/repository';

export const USER_REPOSITORY_QUERY = `query($login:String!, $end_cursor_repository:String, $end_cursor_repository_commit:String, $commits_max:Int) {
    user(login: $login) {
      ${REPOSITORY_FRAGMENT}
    }
  }`;


export const USER_REPOSITORY_COMMIT_QUERY =  `query($login:String!, $end_cursor_repository_commit:String, $repository_name:String!, $commits_max:Int) {
  user(login: $login) {
    repository(name: $repository_name) {
      ref(qualifiedName: "master") {
        target {
          ${COMMIT_FRAGMENT}
        }
      }
    }
  }
}`;
