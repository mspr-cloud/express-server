export const COMMIT_FRAGMENT = `... on Commit {
  history(first: $commits_max, after: $end_cursor_repository_commit) {
    pageInfo {
      endCursor
      hasNextPage
    }
    totalCount
    edges {
      node {
        messageHeadline
        committedDate
        author {
            email
            name
            user {
              login
              avatarUrl
            }
            avatarUrl
        }
      }
    }
  }
}`

export const REPOSITORY_FRAGMENT = `repositories(first: 20, after: $end_cursor_repository, isFork: false, isLocked: false) {
    pageInfo { 
      endCursor
      hasNextPage
    }
    edges {
        node {
            ref(qualifiedName: "master")
            {
                target
                {
                    ${COMMIT_FRAGMENT}
                }
            }
            createdAt
            name
            description
            url
            isFork
            isPrivate
            forkCount
            primaryLanguage {
                name
                color
            }
            stargazers {
                totalCount
            }
            owner {
                login
                avatarUrl
            }
        }
    }
}`
