import { REPOSITORY_FRAGMENT, COMMIT_FRAGMENT } from './fragments/repository.js'

export const PR_USER_FRAGMENT = `...on User{
  pullRequests(first: 50, after: null){
    edges
    {
      node
      {
        repository
        {
          name,
          stargazers{
            totalCount
          }
        }
        title
        state
        closed
        merged
        author
        {
          login
        }
      }
    }
  }
}`


export const ORGANIZATION_MEMBERS_QUERY = `query($organization_name:String!, $end_cursor:String) {
  organization(login: $organization_name) {
    membersWithRole(first: 100, after: $end_cursor) {
      pageInfo {
        endCursor
        hasNextPage
      }
      edges {
        node {
          login
          location
          avatarUrl
          ${PR_USER_FRAGMENT}
        }
      }
    }
  }
}`;

export const ORGANIZATION_REPOSITORIES_QUERY = `query($organization_name:String!, $end_cursor_repository:String, $end_cursor_repository_commit:String, $commits_max:Int) {
  organization(login: $organization_name) {
    ${REPOSITORY_FRAGMENT}
  }
}`;

export const ORGANIZATION_REPOSITORIES_COMMIT_QUERY = `query($organization_name:String!, $end_cursor_repository_commit:String, $repository_name:String!, $commits_max:Int) {
  organization(login: $organization_name) {
    repository(name: $repository_name) {
      ref(qualifiedName: "master") {
        target {
          ${COMMIT_FRAGMENT}
        }
      }
    }
  }
}`;


export const ORGANIZATION_INFORMATION_QUERY = `query($organization_name:String!) {
  organization(login: $organization_name) {
      avatarUrl,
      name,
      description
  }
}`;


