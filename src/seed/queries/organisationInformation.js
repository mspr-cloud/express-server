import gitHubHelper from '../helper/githubGraphQL.js'
import { ORGANIZATION_INFORMATION_QUERY } from '../common/queries/organization.js'

async function executeQuery(orgName) {
  const variables = JSON.stringify({
    organization_name: orgName
  });
  return await gitHubHelper(ORGANIZATION_INFORMATION_QUERY, variables);
}


async function processResult(graph) {
  return graph.data.organization
}

export default (async function(orgName) {
  return await executeQuery(orgName).then((res) => res !== null ? processResult(res) : null);
})

