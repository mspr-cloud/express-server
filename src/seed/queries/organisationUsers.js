import gitHubHelper from '../helper/githubGraphQL.js'
import { ORGANIZATION_MEMBERS_QUERY } from '../common/queries/organization.js'


function getOrganizationUsers (graph) {
  return graph.data.organization.membersWithRole.edges;
}

function organizationUsersHasNextPage (graph) {
  return graph.data.organization.membersWithRole.pageInfo.hasNextPage;
}

function getOrganizationUsersCursor (graph) {
  return graph.data.organization.membersWithRole.pageInfo.endCursor;
}

async function executeQuery(options) {
  const variables = JSON.stringify({
    end_cursor: options.endCursor,
    organization_name: options.orgName
    //pr_max: option.prMax,
    //end_cursor_user_pr: option.cursorUserPr
  });
  return await gitHubHelper(ORGANIZATION_MEMBERS_QUERY, variables);
}


async function processResult(graph, options) {

  let context = getOrganizationUsers(graph);

  while(organizationUsersHasNextPage(graph)) {
    options.endCursor = getOrganizationUsersCursor(graph);
    const newGraph = await executeQuery(options);
    context = [...context, ...getOrganizationUsers(newGraph)]
    graph = newGraph;
  }

  return context.map((edge) => edge.node)
}

export default (async function(orgName) {
  let options = {
    endCursor: null,
    orgName,
  }
  return await executeQuery(options).then(async (res) => res !== null ? await processResult(res, options) : null);
})

