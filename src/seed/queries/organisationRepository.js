import gitHubHelper from '../helper/githubGraphQL.js'
import { ORGANIZATION_REPOSITORIES_QUERY, ORGANIZATION_REPOSITORIES_COMMIT_QUERY } from '../common/queries/organization.js'
import _ from 'lodash'
import processArray from '../helper/processArray'

// Getters
function repositoryHasNextPage(graph) {
  return graph.data.organization.repositories.pageInfo.hasNextPage;
}

function userTotalCommits(acc, curr){
  let login = curr.author.user ? curr.author.user.login : curr.author.name.replace(/\./g,'')
  let avatarUrl = curr.author.user ? curr.author.user.avatarUrl : curr.author.avatarUrl
  if(!acc[login]) {
            acc[login] = {
                login,
                avatarUrl,
                total: 1
            }
        } else {
            acc[login] = {
              login,
              avatarUrl,
              total: acc[login].total + 1
            }
        }
        return acc;
}

function commitHasNextPage(graph) {
  return graph.node.ref.target.history.pageInfo.hasNextPage
}

function getRepositories(graph) {
  return graph.data.organization.repositories.edges
}

function getRepository(graph) {
  return {node: {ref: graph.data.organization.repository.ref }}
}

function getCommits(graph) {
  return graph.node.ref.target.history.edges
}

function getRepositoryCursor(graph) {
  return graph.data.organization.repositories.pageInfo.endCursor;
}

function getCommitCursor(graph) {
  return graph.node.ref.target.history.pageInfo.endCursor;
}

async function executeQuery(options) {
  const variables = JSON.stringify({
    end_cursor_repository: options.endCursorRepository,
    end_cursor_repository_commit: options.endCursorRepositoryCommit,
    organization_name: options.orgName,
    commits_max: options.commitsMax
  });
  return await gitHubHelper(ORGANIZATION_REPOSITORIES_QUERY, variables);
}

async function executeCommitQuery(options) {
  console.log(options.repositoryName)
  const variables = JSON.stringify({
    end_cursor_repository_commit: options.endCursorRepositoryCommit,
    organization_name: options.orgName,
    repository_name: options.repositoryName,
    commits_max: options.commitsMax
  });
  return await gitHubHelper(ORGANIZATION_REPOSITORIES_COMMIT_QUERY, variables);
}

async function processResult(graph, options) {

  let context = getRepositories(graph);

  while(repositoryHasNextPage(graph)) {
    options.endCursorRepository = getRepositoryCursor(graph);
    const newGraph = await executeQuery(options);
    context = [...context, ...getRepositories(newGraph)]
    graph = newGraph;
  }

  return await processArray(context, async (commitGraph) => {
    const initialGraph = commitGraph;
    if (commitGraph.node.ref) {
      let commits = getCommits(commitGraph)
      while (commitHasNextPage(commitGraph)) {
        options.repositoryName = initialGraph.node.name
        options.endCursorRepositoryCommit = getCommitCursor(commitGraph)
        options.commitsMax = 100
        const newCommitGraph = getRepository(await executeCommitQuery(options));
        commits = [...commits, ...getCommits(newCommitGraph)];
        commitGraph = newCommitGraph
      }
      initialGraph.node.commits = commits.map(((commit) => {
        commit.node.committedDate = new Date(commit.node.committedDate);
        return commit.node;
      }));
      initialGraph.node.totalCommit = initialGraph.node.ref.target.history.totalCount;
    }
    initialGraph.node.userTotalCommits = initialGraph.node.commits ? initialGraph.node.commits.reduce(userTotalCommits, {}) : [];
    initialGraph.node.totalStars = initialGraph.node.stargazers.totalCount;
    return _.omit(initialGraph.node, 'ref')
  })
}

export default (async function(orgName) {
  let options = {
    endCursorRepository: null,
    endCursorRepositoryCommit: null,
    orgName,
    repositoryName: null,
    commitsMax: 5
  }
  return await executeQuery(options).then(async (res) => res !== null ? await processResult(res, options) : null);
})
