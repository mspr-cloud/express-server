require('dotenv').config()
import { DB } from '../database';
import blacklistedUsers from '../../blacklist';
import processArray from './helper/processArray';
import organisationUser from './queries/organisationUsers';
import organisationRepository from './queries/organisationRepository';
import organisationInformation from './queries/organisationInformation';
import userRepository from './queries/userRepository';
import seed from './seed';

async function fetchGithubDatas() {

  let root = {
    name: process.env.GITHUB_ORGA,
    date: new Date(),
    repositories: [],
    users: [],
    information: {}
  };

  let information = await organisationInformation(root.name)
  root.information = information;

  let users = await organisationUser(root.name);

  // Remove blacklisted users
  users = users.filter((user) => !blacklistedUsers.includes(user.login))

  await processArray(users, async (user) => {
    let userInfo = await userRepository(user.login);
    if(userInfo) {
      user.repositories = userInfo;
    }
  })

  root.users = users;

  let orgaRepositories = await organisationRepository(root.name);
  root.repositories = orgaRepositories;

  return root;
}

DB.init().then(async () => {
  const root = await fetchGithubDatas();
  await seed(root);
  console.log('Process finished');
  process.exit(0);
})
