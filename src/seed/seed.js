import userSeeder from './seeder/userSeeder';
import statsSeeder from './seeder/statsSeeder';
import organisationSeeder from './seeder/organisationSeeder';
import { DB } from '../database';
export default (async function(data) {

  data = await userSeeder(data);
  data = await organisationSeeder(data);
  data = await statsSeeder(data);


  /**
   * Remove last values
   */
  await DB.$seedHistory.insertOne({
    date: data.date
  }).then(async () => {
    const query = {date: {$lt: data.date}};
    await DB.$commits.remove(query);
    await DB.$repositories.deleteMany(query);
    await DB.$organisationUsers.deleteMany(query);
  });


  console.log('DB SEEDED')
});
