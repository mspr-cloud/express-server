import { DB } from '../../database';
import { findStatsOrganisationLastWeek } from '../helper/da'
import _ from 'lodash'
export default (async function(data) {

  /**
   * Insertion of the most commonly used language on the organization's repositories
   */
  DB.$statsOrganisationLanguageTrends.insertOne({
    date: data.date,
    languages: data.repositories.reduce( (curr, acc) => {
      if(!acc.primaryLanguage) { return curr; }
      acc = acc.primaryLanguage.name
      curr[acc] = (curr[acc] || 0) + 1 ;
      return curr;
    } , {})
  })


  /**
   * Insertion of the users repositories stars
   */

  const usersStarTrendsList = data.users.filter((user) => user.repositories);

  await DB.$statsUsersStarTrends.insertMany(
    usersStarTrendsList.map((user) => ({login: user.login, date: data.date, avatarUrl: user.avatarUrl }))
  ).then(({insertedIds}) => {
    Object.values(insertedIds).forEach((id, index) => {
      const user = usersStarTrendsList[index];
      user.repositories
        .filter((userRepository) => userRepository.owner.login === user.login)
        .forEach((repository) => {
          DB.$statsUsersStarTrendsRepositories.insertOne(
            Object.assign({ stats_users_star_trends_id: id, date: data.date }, _.omit(repository, 'commits'))
          )
        })
    })
  })


  /**
   * Insertion of globals stats of the organization
   */
  const statsOrganisationLastWeek = await findStatsOrganisationLastWeek();
  const organisationRepositoriesTotal = data.repositories.length;
  const organisationUsersTotal = data.users.length;
  const organisationStarsTotal = data.repositories.reduce((acc, repository) => acc + (repository.totalStars || 0), 0);
  const organisationCommitsTotal = data.repositories.reduce((acc, repository) => acc + (repository.totalCommit || 0), 0);
  DB.$statsOrganisation.insertOne({
    date: data.date,
    organisationRepositories: {
      total: organisationRepositoriesTotal,
      progress: statsOrganisationLastWeek.reduce((acc,curr) => {
        acc[curr.date] = curr.organisationRepositories.total;
        return acc;
      }, { [data.date]: organisationRepositoriesTotal })
    },
    organisationUsers: {
      total: organisationUsersTotal,
      progress: statsOrganisationLastWeek.reduce((acc,curr) => {
        acc[curr.date] = curr.organisationUsers.total;
        return acc;
      }, { [data.date]: organisationUsersTotal })
    },
    organisationStars: {
      total: organisationStarsTotal,
      progress: statsOrganisationLastWeek.reduce((acc,curr) => {
        acc[curr.date] = curr.organisationStars.total;
        return acc;
      }, { [data.date]: organisationStarsTotal })
    },
    organisationCommits: {
      total: organisationCommitsTotal,
      progress: statsOrganisationLastWeek.reduce((acc,curr) => {
        acc[curr.date] = curr.organisationCommits.total;
        return acc;
      }, { [data.date]: organisationCommitsTotal })
    }
  })


  return data;

});
