import processArray from '../helper/processArray';
import { DB } from '../../database';
import _ from 'lodash'
export default (async function(data) {

  /**
   * Insertion of the organization's users
   */

  data.users = await processArray(data.users, async (user) => {
    return await DB.$organisationUsers.insertOne(Object.assign({date: data.date}, _.omit(user, 'repositories'))).then(result => {
      user._id = result.insertedId
      return user;
    });
  })

  /**
   * Insertion of the users's repositories
   */
  data.users = await processArray(data.users, async (user) => {
    if(!user.repositories) { return user; }
    user.repositories = await processArray(user.repositories, async (repository) => {
      return await DB.$repositories.insertOne(Object.assign({user_id: user._id, date: data.date, type: 'user'}, _.omit(repository, 'commits'))).then(result => {
        repository._id = result.insertedId
        return repository;
      });
    });
    return user;
  });

  /**
   * Insertion of the users's repositories commits
   */
  data.users = await processArray(data.users, async (user) => {
    if(!user.repositories) { return user; }
    user.repositories = await processArray(user.repositories, async (repository) => {
      if(!repository.commits) { return repository; }
      repository.commits = await processArray(repository.commits, async (commit) => {
        return await DB.$commits.insertOne(Object.assign({repository_id: repository._id, date: data.date, type: 'user'}, commit)).then(result => {
          commit._id = result.insertedId
          return commit;
        });
      });
      return repository;
    });
    return user;
  });

  return data;

});
