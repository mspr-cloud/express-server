import { DB } from '../../database';
import processArray from '../helper/processArray';
import _ from 'lodash'

export default (async function(data) {

  /**
   * Insertion of Organization information
   */
  DB.$organisation.countDocuments().then((count) => {
    if (count === 0) {
      DB.$organisation.insert(data.information)
    } else {
      DB.$organisation.updateMany({}, {$set: data.information})
    }
  })

  /**
   * Insertion of the organization's repositories
   */
  data.repositories = await processArray(data.repositories, async (repository) => {
    return await DB.$repositories.insertOne(Object.assign({date: data.date, type: 'organisation'}, _.omit(repository, 'commits'))).then(result => {
      repository._id = result.insertedId
      return repository;
    });
  });
  /**
   * Insertion of the organization's repositories commits
   */
  data.repositories = await processArray(data.repositories, async (repository) => {
    if(!repository.commits) { return repository; }
    repository.commits = await processArray(repository.commits, async (commit) => {
      return await DB.$commits.insertOne(Object.assign({repository_id: repository._id, date: data.date, type: 'organisation'}, commit)).then(result => {
        commit._id = result.insertedId
        return commit;
      });
    });
    return repository;
  });

  return data;

});
