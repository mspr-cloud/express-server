import { DB } from '../../database';
import Q from 'q';

const DAY_AGO = 6;

export const findStatsOrganisationLastWeek = async () => {
  const deferred = Q.defer();
  DB.$statsOrganisation.find({})
    .sort({"date": -1}).limit(DAY_AGO).toArray(function(err, data) {
      deferred.resolve(data);
    });
  return deferred.promise;
}

