import util from 'util'
import request from 'request-promise'
import { sleep } from './utils';

async function executeQuery(query, variables) {
  await sleep(2000)
  const graphRequest = {
    query,
    variables
  };
  const uri = 'https://api.github.com/graphql';
  return request({
    method: 'POST',
    uri,
    headers: {
      Authorization: `bearer ${process.env.GITHUB_TOKEN}`,
      'User-Agent': 'github-insight'
    },
    body: graphRequest,
    json: true
  }).then((result) => {
    try {
      if (result === undefined) {
        return;
      }
      console.log(`got response: ${util.inspect(result)}`);
      return result;
    } catch (err) {
      const errorMessage = `Failed processing: ${JSON.stringify(graphRequest)}, error: ${err}`;
      console.log(errorMessage);
      return errorMessage;
    }
  })
    .catch((err) => {
      if (err.statusCode === 502) {
        console.log("Retry request because timeout")
        return executeQuery(query, variables)
      }

      const errorMessage = `Could not retrieve graph: ${JSON.stringify(graphRequest)}, error: ${err}`;
      console.log(errorMessage);
      return null;
    });
}

export default (async function(query, variables) {
  return await executeQuery(query, variables)
})

