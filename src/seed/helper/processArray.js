

export default async function processArray(array, cb) {
  let res = []
  for (const item of array) {
     res.push(await cb(item));
  }
  return res;
}
