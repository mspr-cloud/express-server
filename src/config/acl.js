import Acl from 'acl';
import aclStore from '../helper/acl-store';

const MongodbBackend = Acl.mongodbBackend;

export default dbConnection => {
  const backend = new MongodbBackend(dbConnection, 'acl_');
  const acl = new Acl(backend);

  // Set roles
  acl.allow([
    {
      roles: 'admin',
      allows: [
        { resources: '/api/user/:uid/role', permissions: '*' },
        { resources: '/api/user/:uid/delete', permissions: '*' },
        { resources: '/api/user/list', permissions: '*' },
      ]
    },
    {
      roles: 'member',
      allows: [
        { resources: '/api/orga', permissions: '*' },
        { resources: '/api/stats', permissions: '*' },
        { resources: '/api/repository', permissions: '*' },
        { resources: '/api/commits', permissions: '*' },
      ]
    },
    {
      roles: 'user',
      allows: []
    },
    {
      roles: 'guest',
      allows: []
    }
  ]);

  acl.addRoleParents('admin', 'member');
  aclStore.acl = acl;
};
