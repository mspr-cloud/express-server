import aclStore from '../helper/acl-store';
import packageJson from '../../package.json';
import auth from './auth';
import userApi from '../api/user';
import authApi from '../api/auth';
import orgaApi from '../api/orga';
import statsApi from '../api/stats';
import repositoryApi from '../api/repository';
import commitsApi from '../api/commits';
import aclMiddleware from '../middleware/acl';

const API = '/api';

export default app => {
  const { acl } = aclStore;

  /**
   * Public
   */
  app.get(API, (req, res) => {
    res.json({ version: packageJson.version });
  });

  /**
   * Auth Api
   */
  app.use(API, authApi());

  /**
   * Private
   */
  // applies passport authentication on all following routes
  app.all('*', auth.authenticate(), (req, res, next) => next());

  // Protected endpoint
  app.use(`${API}/user`, userApi());

  // Example endpoint only available for 'admin' role
  app.get(`${API}/adminonly`, aclMiddleware, (req, res) => res.sendStatus(200));
  app.use(`${API}/orga`, aclMiddleware, orgaApi());
  app.use(`${API}/stats`, aclMiddleware, statsApi());
  app.use(`${API}/repository`, aclMiddleware, repositoryApi());
  app.use(`${API}/commits`, aclMiddleware, commitsApi());

  // Will return error message as a string -> "Insufficient permissions to access resource"
  app.use(acl.middleware.errorHandler('json'));
};
