FROM node:8

ARG ENV_APP
ENV ENV_APP ${ENV_APP}

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY . .
RUN echo ${ENV_APP} | base64 -d > .env

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production
RUN npm run build
# Bundle app source

EXPOSE 3000
CMD [ "node", "build/server.js" ]
